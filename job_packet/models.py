from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import User
from django.db import models
# Create your models here.
from khayyam import *
from django.utils import timezone
from django.contrib.auth.models import AbstractUser
from job_packet.func import create_time
from django.db import models


class Packet(models.Model):
    pub_date = models.DateTimeField(default=timezone.now)
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.pub_date)


class Job(models.Model):
    heading=models.CharField(max_length=100)
    start_date =models.TimeField(default=timezone.now().strftime('%H:%M'))
    finish_date =models.TimeField(null=True,default=None)
    # lenght_date =models.CharField(null=True,default=None)
    packet = models.ForeignKey(Packet, on_delete=models.CASCADE)
    @property
    def diff(self):
        if self.finish_date==None:
            return None
        t_a = str(self.start_date)
        # print('\n\n\n\n',t_a)
        t_b = str(self.finish_date)

        s = t_a.split(":")
        # g=s[1].split(":")
        s1 = int(s[1]) * 60 + int(s[0])
        s2 = t_b.split(":")
        # g2= s2[1].split(":")
        s3 = int(s2[1]) * 60 + int(s2[0])

        minutesDiff = s1 - s3

        print(minutesDiff)

        h = minutesDiff // 60
        m = minutesDiff - (h * 60)
        s = ""
        if h > 0:
            s = s + str(h) + " ساعت "
        if m > 0:
            s = s + str(m) + " دقیقه "
        if s == "":
            return "0 دقیقه"
        else:
            # print(s)
            return s

    def __str__(self):
        return str(self.heading)


class Client(models.Model):
    name = models.CharField(max_length=128)
    surname = models.CharField(max_length=128)
    phone = models.CharField(max_length=11,help_text='09129999999')
    email= models.EmailField(max_length=128, unique= True)
    picture = models.ImageField(upload_to='static/images/profile_pics', blank=True, default='static/images/profile_pics/new_logo.png')

    def __str__(self):
        return self.name
