from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.show_packets, name='show_packet'),
    url(r'^register/$', views.add_client, name='add_client'),
    url(r'^(?P<pk>\d+)/$', views.packet_detail, name='packet_detail'),
    url(r'^profile/$',views.profile,  name='profile'),
    url(r'^edit_profile/$',views.edit_profile,  name='edit_profile'),
    url(r'^login/$', views.login_user,  name='login'),


]