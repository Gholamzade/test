from django.contrib import admin

# Register your models here.
from django.contrib import admin
from .models import Packet,Job,Client

admin.site.register(Packet)
admin.site.register(Job)
admin.site.register(Client)
