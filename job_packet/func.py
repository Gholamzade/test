from django.utils import timezone
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
from passlib.apps import custom_app_context as pwd_context
import hashlib

def diff(t_a, t_b):
    t_a=str(t_a)
    # print('\n\n\n\n',t_a)
    t_b=str(t_b)

    s=t_a.split(":")
    # g=s[1].split(":")
    s1=int(s[0]) *60 +  int(s[1])
    s2= t_b.split(":")
    # g2= s2[1].split(":")
    s3=int(s2[0]) *60 +  int(s2[1])

    minutesDiff=s1-s3

    # They are now in seconds, subtract and then divide by 60 to get minutes.


    # minutesDiff = int(d2_ts - d1_ts) / 60
    print(minutesDiff)

    h=minutesDiff//60
    m=minutesDiff-(h*60)
    s = ""
    if h>0:
        s=s+ str(h)+ " ساعت "
    if m>0:
        s= s+  str(m) + " دقیقه "
    if s=="":
        return "00:00"
    else:
        # print(s)
        return s



def convert_to_24(time):
    if time[-2:] == "AM" and time[:2] == "12":
        return "00"+time[2:-2]
    elif time[-2:] == "AM":
        return time[:-2]
    elif time[-2:] == "PM" and time[:2] == "12":
        return time[:-2]
    else:
        return str(int(time[:2]) + 12) + time[2:8]



def savePass(passw):
    h = pwd_context.hash(passw)
    return h


def checkPass(passW, h):
    pwd_context.verify(passW, h)
    return pwd_context


def create_time(string):
    t=string.split(" ")
    s=t[1].split(":")
    final=s[0]+":"+s[1]
    return final


def lengh_time(string=str(timezone.now())):
    t=string.split(" ")
    s=t[1].split(":")
    final=s[0]+":"+s[1]

    return final