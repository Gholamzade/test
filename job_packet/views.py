from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.shortcuts import render, render_to_response, HttpResponse
from django.shortcuts import get_list_or_404, get_object_or_404
# Create your views here.
from django.shortcuts import render
from .models import Packet, Job
import time
from django.utils import timezone
from jalali_date import datetime2jalali, date2jalali
from job_packet.forms import TForm,ClientForm
from job_packet.models import Client
from job_packet.func import create_time,diff


def handler404(request):
    return render(request, '404.html', locals())


def handler500(request):
    return render(request, '500.html', locals())


@login_required
def show_packets(request):
    user = get_object_or_404(User, pk=request.user.pk)
    client = get_object_or_404(Client, email=user.username)
    print("url avali")
    if request.method == 'POST' and request.POST.get("form_type") == 'formOne':
        obj = Packet.objects.create(pub_date=timezone.now(), author=request.user)

    packets = Packet.objects.filter(author=request.user).order_by('pub_date')
    if len(packets)==0:
        packets=None
    return render(request, 'job_packet/packets.html', {'packets': packets , 'user':client})


@login_required
def packet_detail(request, pk):
    form = TForm(request.POST)
    packet = get_object_or_404(Packet,author=request.user, pk=pk)

    if request.method == 'POST' and request.POST.get("form_type") == 'formOne':
        obj = Packet.objects.create(pub_date=timezone.now(), author=request.user,)


        return show_packets(request)
    elif request.method == 'POST' and request.POST.get("form_type") == 'formThree':
        objs = Job.objects.filter(packet=packet)
        for i in objs:
            if i.finish_date == None:
                print( timezone.now())
                i.finish_date = timezone.now().strftime('%H:%M')

                i.lenght_date= diff(i.finish_date, i.start_date)
                i.save()

    elif request.method == 'POST':

        objs=Job.objects.filter(packet=packet)
        for i in objs:
            if i.finish_date==None:
                i.finish_date=create_time(str(timezone.now()))
                i.lenght_date= diff(i.finish_date, i.start_date)
                i.save()


        obj = Job.objects.create(heading=form.data['form_type'],start_date=timezone.now().strftime('%H:%M'),  packet=packet)


    try:
        jobsOfPacket = get_list_or_404(Job, packet=packet)
    except:
        jobsOfPacket=None

    packets = Packet.objects.filter(author=request.user).order_by('pub_date')
    user = get_object_or_404(User, pk=request.user.pk)
    client = get_object_or_404(Client, email=user.username)
    return render(request, 'job_packet/full_job_packet.html', {'packets': packets, 'jobsOfPacket': jobsOfPacket ,'user':client })


@login_required
def profile(request):
    user = get_object_or_404(User, pk=request.user.pk)
    client=get_object_or_404(Client, email=user.username)
    form=ClientForm()
    return render(request, 'profile.html', {'form': form, 'client':user , 'user':client})


@login_required
def edit_profile(request):

    user = get_object_or_404(User, pk=request.user.pk)
    client=get_object_or_404(Client, email=user.username)
    temp=client

    if request.method == 'POST':
        form = ClientForm(request.POST)

        if form.is_valid():
            # client = form.save(commit=False)


            print("hi4")
            # print(form.data['username'])
            # print(form.data['password'])
            if  (form.data['username']!= user.username and form.data['password']!= user.password):
                logout(user)
                get_object_or_404(User, username=user.username, password=user.password).delete()
                user = User.objects.create(username=form.data['username'], password=form.data['password'])
                user.save()
                login(request, user)
                client = Client.objects.create( )


            if (form.data['username'] == user.username and form.data['password'] != user.password):
                user.password=form.data['password']
                user.save()

            client.name = form.data['name']
            client.surname = form.data['surname']
            client.email = form.data['username']
            client.phone = form.data['phonenumber']
            try:
                client.picture = request.FILES['picture']
            except:
                client.picture = temp.picture



            client.save()
            return HttpResponseRedirect('/')
    form = ClientForm()
    return render(request, 'client.html', {'form2': form, 'user':client , 'client':user })


def login_user(request):

    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        try:
            user =User.objects.get(username=username, password=password)
            print(user)
            if (user != None):
                login(request,user)
                return HttpResponseRedirect('/')
            else:
                print("inja")
                render_to_response("login.html",
                           {'invalid_login': True})

        except:
            print("excepttttttt")
            return render_to_response( 'login.html', {'next': True})

    return render(request, 'login.html',{'next':False})


def add_client(request):
    user=None

    if request.method == 'POST':
        form = ClientForm(request.POST)

        if form.is_valid():
            client = Client.objects.create(name=form.data['name'], surname=form.data['surname'], email=form.data['username'],phone=form.data['phonenumber'] )
            try:
                print("\n\ninja")
                client.picture = request.FILES['picture']
                print(request.FILES['picture'])
                client.save()
                # print("inja3")
            except:
                print("except")
            print(form.data['username'])
            print(form.data['password'])
            user=User.objects.create(username=form.data['username'], password=form.data['password'])
            user.save()
            login(request, user)

            return HttpResponseRedirect('/profile/')

    form = ClientForm()
    return render(request, 'client.html', {'form2': form,'client':user})
