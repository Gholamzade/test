from django import forms
from jalali_date.fields import JalaliDateField, SplitJalaliDateTimeField
from jalali_date.widgets import AdminJalaliDateWidget, AdminSplitJalaliDateTime
from job_packet.models import Client
from django.forms import CharField, Form, PasswordInput

# class TestForm(forms.ModelForm):
#     class Meta:
#         model = TestModel
#         fields = ('name', 'date', 'date_time')
#
#     def __init__(self, *args, **kwargs):
#         super(TestForm, self).__init__(*args, **kwargs)
#         self.fields['date'] = JalaliDateField(label=_('date'),
#             widget=AdminJalaliDateWidget # optional, for user default datepicker
#         )
#         # you can added a "class" to this field for user your datepicker!
#         # self.fields['date'].widget.attrs.update({'class': 'jalali_date-date'})
#
#         self.fields['date_time'] = SplitJalaliDateTimeField(label=_('date time'),
#             widget=AdminSplitJalaliDateTime # required, for decompress DatetimeField to JalaliDateField and JalaliTimeField
#         )

class ClientForm(forms.Form):
    # email= forms.CharField()
    # # username = forms.CharField(max_length=128)
    # password = forms.CharField(widget=PasswordInput())
    # name = forms.CharField()
    # surname = forms.CharField()
    # phone = forms.IntegerField()
    # # address = forms.CharField(max_length=128, help_text="Address: ", required=False)
    # # desired_weight = forms.IntegerField(help_text="Desired Weight: ")
    # picture = forms.ImageField(help_text="Upload image: ", required=False)
    # start_weight = forms.IntegerField(help_text="Start Weight: ")
    # views = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    # likes = forms.IntegerField(widget=forms.HiddenInput(), initial=0)
    # slug = forms.CharField(widget=forms.HiddenInput(), required=False)
    # comments = forms.CharField(max_length=500, help_text="Comments: ", required=False)

    # An inline class to provide additional information on the form.
    class Meta:
        # Provide an association between the ModelForm and a model
        model = Client

        # fields = ('name','surname','phone')
        # fields = ('name','surname','phone', 'picture')

# class UserForm(forms.ModelForm):
#     password = forms.CharField(widget=forms.PasswordInput())
#
#     class Meta:
#         model = auth.user
#         fields = ('username', 'email', 'password')
#
# class UserProfileForm(forms.ModelForm):
#     class Meta:
#         model = UserProfile
#         fields = ('nickname',)
# #        fields = ('website', 'picture')
#


class TForm(forms.Form):
    class Meta:
       pass

       # class ImageUploadForm(forms.Form):
       #     """Image upload form."""
       #     image = forms.ImageField()

# class changeForm(forms.Form):
#     def save(self, commit=True):
#         user = super(changeForm, self).save(commit=False)
#         if user.avatar:  # If the form includes an avatar
#             user.set_avatar()  # Use this bool to check in templates
#         if commit:
#             user.save()
#         return user
