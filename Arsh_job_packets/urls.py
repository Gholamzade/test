"""Arsh_job_packets URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
# from . import views

from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from job_packet.views import profile
from django.contrib.auth import views as auth_views
from django.conf.urls import (
handler400, handler403, handler404, handler500
)
import django.views.defaults
# handler400 = 'job_packet.views.handler404'
# handler403 = 'my_app.views.permission_denied'
handler404 = ' job_packet.views.handler404'
handler500 = 'job_packet.views.handler500'

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'', include('job_packet.urls')),
    # url(r'^404/$',django.views.defaults.page_not_found,),
    # url(r'^500/$',django.views.defaults.server_error,),
    # url(r'^logout/$', auth_views.logout, name='logout'),
    url(r'^logout/$', auth_views.logout, {'next_page': '/login/'}, name='logout'),
]